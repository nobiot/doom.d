;; publish.el --- Publish org-mode project on Gitlab Pages
;;
;; Original Author: Rasmus
;; Source: https://gitlab.com/pages/org-mode/-/blob/master/publish.el
;;
;; Adaptation by me based on this tutorial:
;; https://opensource.com/article/20/3/blog-emacs
;;

;;; Commentary:
;; This script will convert the org-mode files in this directory into
;; html.

;;; Code:

(require 'package)
(package-initialize)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'org-plus-contrib)
(package-install 'htmlize)

(require 'org)
(require 'ox-publish)

;; setting to nil, avoids "Author: x" at the bottom
(setq user-full-name nil)

(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc nil)

(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-doctype "html5"
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link nil
      org-html-head-include-default-style nil; removing the default style
      org-html-head-include-scripts nil); removing the code-highlighter
                                        ; default javascript

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"
                "css"))
  "File types that are published as static files.")

(defvar site-head-links "<link rel=\"stylesheet\" type=\"text/css\" href=\"./static/style/tufte.css\"/>
<link href=\"https://fonts.googleapis.com/css?family=IBM+Plex+Mono|Roboto&display=swap\" rel=\"stylesheet\"/>")

(setq org-publish-project-alist
      (list
       (list "posts"
             :base-directory "site/posts/"
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public/"
             :exclude (regexp-opt '("README" "draft"))
             :auto-sitemap t
             :sitemap-title "Emacs Tech Notes"
             :sitemap-filename "index.org"
             :sitemap-file-entry-format "%d *%t*"
             :html-head site-head-links
             ;:html-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"./static/style/tufte.min.css\"/>"
             ;:html-head-extra "<link href=\"https://fonts.googleapis.com/css?family=Nanum+Gothic+Coding&display=swap\" rel=\"stylesheet\">"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"./static/favicon.ico\"/>"
             :sitemap-style 'list
             :sitemap-sort-files 'anti-chronologically)
       (list "static"
             :base-directory "site/static/"
             :exclude "public/"
             :base-extension site-attachments
             :publishing-directory "./public/static/"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "all" :components '("static" "posts"))))

(provide 'publish)
;;; publish.el ends here
