# Makefile for testing the publish on local machine
# Source: https://opensource.com/article/20/3/blog-emacs
# Make sure to modify publish.el (comment out the section)
# where package installs =org= etc.
# Obvously, for my local mahcine that is not needed.
# Ionly use publish; the others are left as they are
# for references.

.PHONY: all publish publish_no_init

all: publish

publish: publish.el
        @echo "Publishing... with current Emacs configurations."
        emacs --batch --load publish.el --funcall org-publish-all

publish_no_init: publish.el
        @echo "Publishing... with --no-init."
        emacs --batch --no-init --load publish.el --funcall org-publish-all

clean:
        @echo "Cleaning up.."
        @rm -rvf *.elc
        @rm -rvf public
        @rm -rvf ~/.org-timestamps/*
