My config files under .dooom.d for Doom Emacs.

Emacs related notes are published on GitLab pages with using `ox-publish` from
within this directory.

URL: https://nobiot.gitlab.io/doom.d/
(I am thinking of mapping it to my `nobiot` domain, perhaps `emacs.nobiot.com`).

All done in Windows. 