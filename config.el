;(setq doom-variable-pitch-font (font-spec :family "iA Writer Quattro S" :size 14))
;(setq doom-big-font (font-spec :family "iA Writer Quattro S" :size 14))

(global-set-key [M-kanji] 'ignore)
(global-set-key [kanji] 'ignore)

(set-face-attribute 'variable-pitch nil  :family "iA Writer Quattro S" :height 110)
(set-face-attribute 'default nil  :family "iA Writer Mono S" :height 110)
(set-fontset-font nil 'japanese-jisx0208 (font-spec :family "HackGen35 Console"))
(set-fontset-font nil 'japanese-jisx0213.2004-1 (font-spec :family "HackGen35 Console"))
(set-fontset-font nil 'japanese-jisx0213-2 (font-spec :family "HackGen35 Console"))
(set-fontset-font nil 'symbol (font-spec :family "Segoe UI Symbol" :size 11.0))
;; Below is a subset of 'symbol that represent emoji. 0x1F333 (hexademimal 1F333) = 127795
(set-fontset-font nil '(127795 . 128450) (font-spec :family "Segoe UI Emoji" :size 12.0))
;; "🔒" is UI Symbol
(set-fontset-font nil '(128274 . 128274) (font-spec :family "Segoe UI Symbol" :size 12.0))

(setq custom-safe-themes t)
(load-theme 'less)

(setq modus-operandi-theme-slanted-constructs nil
      modus-operandi-theme-bold-constructs t
      modus-operandi-theme-visible-fringes nil
      modus-operandi-theme-3d-modeline nil
      modus-operandi-theme-subtle-diffs t
      modus-operandi-theme-distinct-org-blocks nil
      modus-operandi-theme-proportional-fonts nil
      modus-operandi-theme-rainbow-headings nil
      modus-operandi-theme-section-headings t
      modus-operandi-theme-scale-headings nil)
;(load-theme 'modus-operandi t)

(setq window-divider-default-bottom-width 0)

(setq display-line-numbers-type nil)

(use-package! orderless
  :init (icomplete-mode)
  :custom (completion-styles '(orderless))
  :config
  (setq orderless-component-separator "[ &]")
  ;;(setq orderless-matching-styles '(orderless-regexp orderless-initialism))
  ;; https://github.com/oantolin/orderless#company
  ;; The matching portions of candidates aren’t highlighted.
  ;; That’s because company-capf is hard-coded to look for
  ;; the completions-common-part face, and it only use
  ;; one face, company-echo-common to highlight candidates.
  (defun my/just-one-face (fn &rest args)
    (let ((orderless-match-faces [completions-common-part]))
      (apply fn args)))

  (advice-add 'company-capf--candidates :around #'my/just-one-face))

;(use-package! ivy
;  :config (setq ivy-re-builders-alist '((t . orderless-ivy-re-builder))))

(turn-off-auto-fill)

(defun my/mode-line-vc-state ()
  (when (buffer-file-name)
    (cond ((eq 'edited (vc-state buffer-file-name)) "⇄")
          ((eq 'up-to-date (vc-state buffer-file-name)) "✓")
          (t))))

(defun my/mode-line-vc-mode ()
  (when (stringp vc-mode)
    (substring-no-properties vc-mode 5 nil)))

(defun my/simple-mode-line-render (left right)
  "Return a string of `window-width' length containing LEFT, and
RIGHT aligned respectively."
  (let* ((available-width (- (window-total-width)
                             (length left)
                             5)))
    (format (format " %%s %%%ds " available-width) left right)))

(defun my/mode-line-left ()
  (format-mode-line '(
                      "%*"
                      " "
                      mode-line-buffer-identification
                      " "
                      mode-name
                      " "
                      mode-line-frame-identification
                      )))
(defun my/mode-line-right ()
  (format-mode-line '(
                      "%l:%C"
                      "  "
                      (:eval (my/mode-line-vc-state))
                      " "
                      (:eval (my/mode-line-vc-mode))
                      "  "
                      mode-line-misc-info)))

(use-package! emacs
  :config
  (setq mode-line-percent-position '(-3 "%p"))
  (setq mode-line-defining-kbd-macro
        (propertize " Macro" 'face 'mode-line-emphasis))
  (setq column-number-mode t)
  (setq-default mode-line-format
      '("%e"
        mode-line-front-space
        mode-line-client
        (:eval (my/simple-mode-line-render
                ;; left
                (my/mode-line-left)
                ;; right
                (my/mode-line-right)))
        mode-line-end-spaces)))

(setq display-time-day-and-date t)
(setq display-time-string-forms
      '((propertize (format-time-string "%H:%M %Y-%m-%d"))))
(display-time)

(use-package flycheck-indicator
  :after flycheck
  :config
  (setq flycheck-indicator-icon-error (string-to-char "!"))
  (setq flycheck-indicator-icon-info (string-to-char "·"))
  (setq flycheck-indicator-icon-warning (string-to-char "*"))
  (setq flycheck-indicator-status-icons
        '((not-checked "*")
          (no-checker "-")
          (running "⟳")
          (errored "!")
          (finished "🎔")
          (interrupted "∅")
          (suspicious "‽")))
  :hook (flycheck-mode-hook . flycheck-indicator-mode))

(setq org-directory deft-directory)
(setq my/journal-directory "~/iCloudDrive/home/journal")
(setq my/fleeting-notes-directory "~/iCloudDrive/home/fleeting")
(setq my/permanent-notes-directory "~/iCloudDrive/home/evergreen")
(setq my/literature-notes-directory "~/iCloudDrive/home/evergreen/lit") ;under org-roam directory

(use-package! org
  :defer t
  :init
  (setq org-agenda-files (list my/journal-directory))
  :config
  (setq org-superstar-headline-bullets-list '("🙪" "➙" "•" "‣" "⁍" "◦"))
  (setq org-superstar-special-todo-items t)
  (setq org-use-speed-commands t)
  (add-to-list 'org-speed-commands-user '("d" org-todo "DONE"))
  (add-to-list 'org-speed-commands-user '("k" org-todo "KILL"))
  (add-to-list 'org-speed-commands-user '("w" widen))
  (add-to-list 'org-speed-commands-user '("z" org-narrow-to-subtree))
  (add-to-list 'org-speed-commands-user '("N" org-narrow-to-subtree))
  (my/org-init-capture) ; to change the org-capture's template from doom's defaults
  (setq +org-capture-todo-file "~/iCloudDrive/home/journal/todo.org")
  (setq +org-capture-notes-file "~/iCloudDrive/home/fleeting/notes.org")
  ;; org-journal-dir and org-agenda-files both might need to be set it is "required"
  ;; in my case, in use-package! org -- if init, it is loaded
  ;; at the time of initial load (i think)
  ;; https://github.com/bastibe/org-journal/issues/9
  ;; (require 'org-journal)
  (setq org-todo-keywords
      '((sequence "TODO" "IN-PROGRESS" "|" "DONE" "KILL"))))

(defun my/org-journal-find-location ()
  ;; Open today's journal, but specify a non-nil prefix argument in order to
  ;; inhibit inserting the heading; org-capture will insert the heading.
  (org-journal-new-entry t)
  ;; Position point on the journal's top-level heading so that org-capture
  ;; will add the new entry as a child entry.
  (goto-char (point-min)))

(defun my/org-init-capture ()
 (setq org-default-notes-file
        (expand-file-name +org-capture-notes-file org-directory)
        +org-capture-journal-file
        (expand-file-name +org-capture-journal-file org-directory)
        org-capture-templates
        '(("t" "Todo" entry
           (file+headline +org-capture-todo-file "Inbox")
           "* [ ] %?\n%i\n%a" :prepend t))))

(defvar my/screenshot-directory "./images/")
(defun my/take-screenshot ()
  "This works only in Windows. Call a clipping tool and take a screenshot.
   Store in png format into 'my/screenshot-directory"
   (setq filename
        (concat
         (make-temp-name
          (concat my/screenshot-directory
                  (format-time-string "%Y%m%d%H%M")) ) ".png"))
  (shell-command "snippingtool /clip")
  (shell-command (concat "powershell -command \"Add-Type -AssemblyName System.Windows.Forms;if ($([System.Windows.Forms.Clipboard]::ContainsImage())) {$image = [System.Windows.Forms.Clipboard]::GetImage();[System.Drawing.Bitmap]$image.Save('" filename "',[System.Drawing.Imaging.ImageFormat]::Png); Write-Output 'clipboard content saved as file'} else {Write-Output 'clipboard does not contain image data'}\"")))

(defun my/org-screenshot ()
  "Take a screenshot into a time stamped unique-named file in the
same directory as the org-buffer and insert a link to this file."
  (interactive)
  (my/take-screenshot)
  (insert (concat "[[file:" filename "]]")))

(defun my/md-screenshot ()
  "Take a screenshot into my/screenshot-directory
Insert a markdown image link"
  (interactive)
  (my/take-screenshot)
  (insert (concat "![]("filename")")))

(use-package! org-journal
  :defer t
  :config
  (setq org-journal-dir my/journal-directory)
  (setq org-journal-date-format "%A, %d %B %Y")
  (setq org-journal-file-type 'monthly)
  (setq org-journal-file-format "%Y%m.org")
  (setq org-journal-enable-agenda-integration nil)
  (setq org-journal-cache-file "~/.emacs.d/.local/cache/org-journal.cache"))

(setq org-clock-history-length 23)

(defun eos/org-clock-in ()
  (interactive)
  (org-clock-in '(4)))

(defun org-journal-find-location ()
  (interactive "p")
  ;; Open today's journal, but specify a non-nil prefix argument in order to
  ;; inhibit inserting the heading; org-capture will insert the heading.
  (org-journal-new-entry t)
  ;; Position point on the journal's top-level heading so that org-capture
  ;; will add the new entry as a child entry.
  (goto-char (point-min)))

(use-package! org-roam
  :commands (org-roam-insert org-roam-find-file org-roam-buffer-toggle-display org-roam-mode)
  :init
  (setq org-roam-directory deft-directory)
  (setq org-roam-file-extensions '("md" "org"))
  (setq org-roam-list-files-commands '(rg elisp))
  (setq org-roam-title-sources '((mdtitle mdheadline) (mdalias)))
  (setq org-roam-tag-sources '(md-frontmatter))
  (setq org-roam-tag-separator " ")
  (setq org-roam-rename-file-on-title-change nil) ; not necessary, as I'm removeing fn rg-roam--update-file-name-on-title-change
  (setq org-roam-title-change-hook '(org-roam--update-links-on-title-change))
  (map! :leader
        :prefix "n"
        :desc "Org-Roam-Insert" "i" #'org-roam-insert
        :desc "Org-Roam-Find"   "/" #'org-roam-find-file
        :desc "Org-Roam-Toggle" "r" #'org-roam-buffer-toggle-display)
  (setq org-roam-capture-templates '(("d" "default" plain (function org-roam--capture-get-point) "%?" :file-name "%<%Y%m%d%H%M%S>" :head "#+TITLE: ${title}
   " :unnarrowed t))))

(use-package! md-roam ; load immediately, before org-roam
  :config
  (setq md-roam-file-extension-single "md")
  (setq md-roam-use-org-extract-ref nil)
  (setq md-roam-use-org-file-links t)

;; The following Org-roam variables may interfere with Md-roam
  (setq org-roam-enable-fuzzy-links t)
  (setq org-roam-auto-replace-fuzzy-links nil))

(after! org-roam
  (set-company-backend! 'org-mode 'company-capf 'company-org-roam)
  (set-company-backend! 'markdown-mode 'company-capf 'company-org-roam))

(defun my/create-new-fleeting-note ()
  "Create a new fleeting note in 'my/fleeting-note-directory.
The file name is my time stamp format with 'md' extension."
  (interactive)
  (require 'deft)
  (let ((file (concat (file-name-as-directory (expand-file-name my/fleeting-notes-directory))
                      (deft-unused-slug) "." deft-default-extension)))
    (if (file-exists-p file)
        (message "Aborting, file already exists: %s" file)
      (find-file-other-window file))))

(add-hook 'dired-mode-hook
          (lambda ()
            (dired-hide-details-mode 1)))

(use-package! dired
  :config
  (defun my/dired-open-fleeting-notes-dir ()
    "Open and switch to `my/fleeting-notes-directory'."
    (interactive)
    (require 'ido)
    (dired (ido-expand-directory my/fleeting-notes-directory))))

(use-package! dired-subtree
  :after dired
  :commands(dired)
  :config
  (setq dired-subtree-use-backgrounds nil)
  (defun my/dired-display-next-file ()
    (interactive)
    (dired-next-line 1)
    (dired-display-file)
    (when (org-roam-backlinks-mode)
      (setq org-roam-buffer--current (find-file-noselect (dired-get-file-for-visit)))
      (org-roam-buffer-update)))
  (defun my/dired-display-prev-file ()
    (interactive)
    (dired-previous-line 1)
    (dired-display-file)
    (when (org-roam-backlinks-mode)
      (setq org-roam-buffer--current (find-file-noselect (dired-get-file-for-visit)))
      (org-roam-buffer-update)))
  :bind (:map dired-mode-map
         ("<tab>" . dired-subtree-toggle)
         ("<C-tab>" . dired-subtree-cycle)
         ("<SPC>" . my/dired-display-next-file)
         ("<down>" . my/dired-display-next-file)
         ("<up>" . my/dired-display-prev-file)))

(use-package! rg
  :config
  (rg-define-search my/rg-fleeting-notes
    "RipGrep in my fleeting notes directory."
    :query ask
    :format regexp
    :files "everything"
    :dir my/fleeting-notes-directory
    :confirm prefix)
  (rg-define-search my/rg-permanent-notes
    "RipGrep in my permanent notes directory."
    :query ask
    :format regexp
    :files "everything"
    :dir my/permanent-notes-directory
    :confirm prefix))

(use-package visual-regexp
  :commands(vr/replace vr/query-replace)
  :config
  (setq vr/default-replace-preview t)
  (setq vr/match-separator-use-custom-face nil))

(use-package! re-builder
  :defer t
  :config
  (setq reb-re-syntax 'read))

(add-hook 'text-mode-hook 'abbrev-mode)
(add-hook 'text-mode-hook 'visual-line-mode)
(add-hook 'text-mode-hook 'olivetti-mode)
(add-hook 'text-mode-hook 'pandoc-mode)
(remove-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'markdown-mode-hook 'abbrev-mode)
(add-hook 'markdown-mode-hook 'visual-line-mode)
(add-hook 'markdown-mode-hook 'olivetti-mode)
(add-hook 'markdown-mode-hook 'my/turn-off-line-numbers)
(add-hook 'markdown-mode-hook 'flymake-mode nil)
(add-hook 'markdown-mode-hook 'turn-off-auto-fill)
;(add-hook 'markdown-mode-hook 'my/doom-modeline-cust)
(add-hook 'org-mode-hook  'abbrev-mode)
(add-hook 'org-mode-hook 'visual-line-mode)
(add-hook 'org-mode-hook 'olivetti-mode)
(add-hook 'org-mode-hook 'my/turn-off-line-numbers)
(add-hook 'org-mode-hook 'flymake-mode nil)
(add-hook 'org-mode-hook 'turn-off-auto-fill)
;(add-hook 'org-mode-hook 'my/doom-modeline-cust)
(add-hook 'emacs-lisp-mode-hook 'display-line-numbers--turn-on)
(add-hook 'emacs-lisp-mode-hook 'my/turn-on-line-numbers)
;(add-hook 'deft-mode-hook 'org-roam-mode)
(remove-hook 'deft-mode-hook 'display-line-numbers--turn-on)

(defun my/turn-on-line-numbers()
  (when (eq display-line-numbers nil)
    (setq display-line-numbers-type t)
    (display-line-numbers-mode)))

(defun my/turn-off-line-numbers()
  (when (eq display-line-numbers t)
    (setq display-line-numbers-type nil)
    (display-line-numbers-mode)))

(define-abbrev-table 'global-abbrev-table
  '(("sct" "§")
    ("poundsym" "£")
    ("eurosym" "€"))
  ) ;section §

(use-package! flyspell
  :commands (flyspell-mode)
  :init
  (setenv "LANG" "en_GB")
  :config
  ;; This below has made the hunspell to work; no longer fly-spell issues
  ;; https://emacs.stackexchange.com/questions/30008/hunspell-flyspell-and-emacs-on-windows
  (setq ispell-program-name
        "~/bin/hunspell-1.3.2-3-w32-bin/bin/hunspell.exe")
  (setq ispell-dictionary "en_GB")
  (setq ispell-dictionary-alist
        '(("en_GB" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "en_GB") nil utf-8))))

(use-package! helm-bibtex
  :commands helm-bibtex
  :config
  (setq helm-display-buffer-width nil)
  (setq helm-buffer-max-len-mode 1)
  (setq helm-buffer-max-length nil)
  (setq bibtex-completion-bibliography "~/iCloudDrive/home/bibliography/myBibliography.bib")
  (setq bibtex-completion-notes-path my/literature-notes-directory)
  ;(setq bibtex-completion-library-path "~/iCloudDrive/home/bibliography/pdf")
  (setq bibtex-completion-pdf-field "File")
  (setq bibtex-completion-notes-extension ".md")
  (setq bibtex-completion-pdf-symbol "⌘")
  (setq bibtex-completion-notes-symbol "✎")
  ;; I want Pandoc style citations even in org-mode
  (setq bibtex-completion-format-citation-functions
        '((org-mode . bibtex-completion-format-citation-default)
          (markdown-mode . bibtex-completion-format-citation-pandoc-citeproc)
          (defaul . bibtex-completion-format-citation-default)))
  ;; I want default action (by pressing enter) to be insert citation key
  ;; Other options can be opend via `M-o` -> May need ivy-hydra config
  (helm-delete-action-from-source "Insert citation" helm-source-bibtex)
  (helm-add-action-to-source
   "Insert citation" 'helm-bibtex-insert-citation helm-source-bibtex 0))

(use-package! ivy-bibtex
  :commands ivy-bibtex
  :config
  (setq ivy-re-builders-alist
      '((ivy-bibtex . ivy--regex-ignore-order)
        (t . ivy--regex-plus)))
  (ivy-set-actions
   'ivy-bibtex
   '(("p" ivy-bibtex-open-any "Open PDF, URL, or DOI")
     ("e" ivy-bibtex-edit-notes "Edit notes")
     ("b" ivy-bibtex-show-entry "Show bib entry")))
  (setq bibtex-completion-bibliography "~/iCloudDrive/home/bibliography/myBibliography.bib")
  (setq bibtex-completion-notes-path my/literature-notes-directory)
  ;(setq bibtex-completion-library-path "~/iCloudDrive/home/bibliography/pdf")
  (setq bibtex-completion-pdf-field "File")
  (setq bibtex-completion-notes-extension ".md")
  (setq bibtex-completion-pdf-symbol "⌘")
  (setq bibtex-completion-notes-symbol "✎")
  ;; I want Pandoc style citations even in org-mode
  (setq bibtex-completion-format-citation-functions
        '((org-mode . bibtex-completion-format-citation-default)
          (markdown-mode . bibtex-completion-format-citation-pandoc-citeproc)
          (defaul . bibtex-completion-format-citation-default)))
  ;; I want default action (by pressing enter) to be insert citation key
  ;; Other options can be opend via `C-o`  or 'M-o' -> May need ivy-hydra config))
  (setq bibtex-completion-notes-template-multiple-files
        "---
title: ${author-or-editor} (${year}), ${title}
date:
roam_key: ${=key=}
---"))

(use-package! olivetti
  :defer t
  :init
  (setq olivetti-body-width 80)
)

(define-key global-map "\C-z" 'undo)
(define-key global-map "\M-z" 'undo-fu-only-redo)
(define-key global-map "\M-t" 'toggle-truncate-lines)
;(define-key global-map "\C-w" 'backward-kill-words
(define-key global-map "\C-d" 'kill-word)
(define-key global-map "\M-d" 'delete-char)
(define-key global-map "\C-o" 'other-window)
;(Define-key global-map "\C-s" 'swiper-isearch)
;(map! :map global-map "<escape>" #'god-mode-all)
(map! :map global-map
      "<f5>" #'my/deft-start) ;this needs to be at init; otherwise, f5 won't work until after deft has started.
(map! :map global-map "<f6>" #'ivy-bibtex)
(map! :map global-map "<f7>" #'my/dired-open-fleeting-notes-dir)
(map! :map global-map "<f8>" #'my/deft-toggle-directory)
(map! :map global-map "<f12>" #'magit-status)
(map! :map global-map "C-c C-j" #'org-journal-new-entry)
(global-set-key (kbd "C-c C-I") #'eos/org-clock-in)
(global-set-key (kbd "C-c C-i") #'org-clock-in)
(global-set-key (kbd "C-c C-o") #'org-clock-out)
(map! :map global-map "C-c C-n" #'my/create-new-fleeting-note)

;;; racket-mode
(map! :map racket-mode-map
      "C-," "◊"
      "C-." "◊")

(set-default 'cursor-type '(bar . 2)) ; << set in the theme
