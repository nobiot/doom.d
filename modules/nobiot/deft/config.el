;;; ui/deft/config.el -*- lexical-binding: t; -*-

(use-package! deft
  :commands deft
  :init
  (defun my/deft-start ()
    "Custom function to start deft, immediately followed by deft-refresh to refresh notes list."
    (interactive)
    (deft)
    (deft-refresh))
  (setq deft-directory "~/iCloudDrive/home/evergreen")
  (setq deft-default-extension "md"
        ;; de-couples filename and note title:
        deft-use-filename-as-title t ; file name (without the extension)
                                     ; represents note's id
        deft-use-filter-string-for-filename nil
        ;; converts the filter string into a readable file-name using kebab-case:
        ;; nobiot: I use only auto generated filename with ISO 8601 like:
        ;; 2020-0320T113023 << This "T" should be upper case for me
        deft-file-naming-rules
        '((noslash . "-")
          (nospace . "-")))
  :config
  (setq deft-new-file-format "%Y-%m-%dT%H%M%S")
  (setq deft-auto-save-interval 0)
  (setq deft-strip-summary-regexp
        (concat "\\(?:"
          "^%+" ; line beg with %
          "\\|^#\\+TITLE: *" ; org-mode title
          "\\|^[#* ]+" ; line beg with #, * and/or space
          "\\|-\\*-[[:alpha:]]+-\\*-" ; -*- .. -*- lines
          "\\|^Title:[\t ]*" ; MultiMarkdown metadata
          "\\|#+" ; line with just # chars
          "\\|-\\{3\\}[[:blank:]]*" ; yaml frontmatter boarder
          "\\|title:[\t ]*" ; yaml frontmatter key:
          "\\|[[:blank:]]*[️︎][[:blank:]]*" ; unicode variant-selectors (this might misbehave for mathmatic notations);
                 ; https://codepoints.net/variation_selectors
          "\\)"))
  (add-hook 'deft-mode-hook #'doom-mark-buffer-as-real-h)
  ;; start filtering immediately
  (set-evil-initial-state! 'deft-mode 'insert)
  (map! :map deft-mode-map
        :n "gr"  #'deft-refresh
        :n "C-s" #'deft-filter
        :i "C-n" #'deft-new-file
        :i "C-m" #'deft-new-file-named
        :i "C-d" #'deft-delete-file
        :i "C-r" #'deft-rename-file
        :n "r"   #'deft-rename-file
        :n "a"   #'deft-new-file
        :n "A"   #'deft-new-file-named
        :n "d"   #'deft-delete-file
        :n "D"   #'deft-archive-file
        :n "q"   #'kill-current-buffer
        :localleader
        "RET" #'deft-new-file-named
        "a"   #'deft-archive-file
        "c"   #'deft-filter-clear
        "d"   #'deft-delete-file
        "f"   #'deft-find-file
        "g"   #'deft-refresh
        "l"   #'deft-filter
        "n"   #'deft-new-file
        "r"   #'deft-rename-file
        "s"   #'deft-toggle-sort-method
        "t"   #'deft-toggle-incremental-search))

(defun my/deft-toggle-directory ()
  (interactive)
  (when-let ((buf (get-buffer "*Deft*")))
    (kill-buffer buf))

  ;; Swaping the deft-directory between permanent notes and fleeting notes
  (if (string= deft-directory my/permanent-notes-directory)
      (setq deft-directory my/fleeting-notes-directory)
    (setq deft-directory my/permanent-notes-directory))
  (deft)
  )


(use-package! zetteldeft
  ;; Setting zetteldeft-id-format needs to be at init.
  ;; Inside zetteldeft, it does (setq deft-new-file-format zeteldef-id-fomat),
  ;; overriding all the customization to this deft variable!
  ;; It took me about 4 calendar days of effort to understand this problem.
  ;; If done at init, zetteldeft is free to set the deft variable like this.
  :init
  (setq zetteldeft-id-format deft-new-file-format)
  (setq zetteldeft-id-regex "[0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}T[0-9]\\{6\\}")
  :after deft)
